import React from "react";
import styles from "./text.css";
import classNames from "classnames";

type TSizes = 28 | 20 | 16 | 14 | 12 | 10;
type TWeights = 400 | 500 | 700;

export enum EColor {
  black = "black",
  orange = "orange",
  green = "green",
  white = "white",
  grayF4 = "grayF4",
  grayF3 = "grayF3",
  grayC4 = "grayC4",
  gray99 = "gray99",
  gray66 = "gray66",
  grayD9 = "grayD9",
}

interface IText {
  As?: "span" | "h1" | "h2" | "h3" | "h4" | "p" | "div";
  children?: React.ReactNode;
  size?: TSizes;
  mobileSize?: TSizes;
  tabletSize?: TSizes;
  desktopSize?: TSizes;
  color?: EColor;
  className?: string;
  weight?: TWeights 
  margin?: 16 | 14 | 12 | 10 | 0
}

export function Text({
  As = "span",
  children,
  size = 16,
  weight = 400,
  mobileSize,
  tabletSize,
  desktopSize,
  color = EColor.black,
  className = "",
  margin = 0
}: IText) {
  // const classes = classNames(styles[`s${size}`], { styles[`m${mobileSize}`]: mobileSize})
  const classes = classNames(styles[`s${size}`],styles[`mb${margin}`], styles[color], styles[`w${weight}`], {
    [styles[`m${mobileSize}`]]: mobileSize,
    [styles[`t${tabletSize}`]]: tabletSize,
    [styles[`d${desktopSize}`]]: desktopSize,
  });

  return <As className={[classes, className].join(" ")}>{children}</As>;
}
