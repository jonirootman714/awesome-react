import React, { ChangeEvent, useEffect, useRef } from "react";
// import { useDispatch, useSelector } from "react-redux";
import { CommentForm } from "../Post/CommentForm";
// import { RootState } from "../../store/store";
// import { updateComment } from "../../store/comment/actions";
import { FormikHelpers } from "formik";
import { useStoreon } from "storeon/react";

interface ICommentFormContainer {
  buttonSubmitText?: string;
  placeholder?: string;
  initialValue?: string;
}

export interface ICommentValues {
  comment: string;
}

export function CommentFormContainer({
  buttonSubmitText = "Submit",
  placeholder = "",
  initialValue = "",
}: ICommentFormContainer) {
  const { dispatch, commentText } = useStoreon("commentText");

  // const value = useSelector<RootState, string>((state) => state.commentText);
  // const dispatch = useDispatch();

  const ref = useRef<HTMLTextAreaElement>(null);

  useEffect(() => {
    if (ref.current !== null) {

      // dispatch(updateComment(initialValue));

      // dispatch("setComment", );
      ref.current.focus();
    }
  }, []);

  function handleSubmit(
    values: ICommentValues,
    { setSubmitting }: FormikHelpers<ICommentValues>
  ) {
    setTimeout(() => {
      alert(JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 500);
  }

  function handleChange(event: ChangeEvent<HTMLTextAreaElement>) {
    // dispatch(updateComment(event.target.value));
    console.log('change event', event.target.value)
    dispatch("setComment", event.target.value);
  }

  return (
    <CommentForm
      onSubmit={handleSubmit}
      onChange={handleChange}
      textareaRef={ref}
      buttonSubmitText={buttonSubmitText}
      placeholder={placeholder}
      initialValue={commentText}
    />
  );
}
