import React from 'react';
import styles from './content.css';

interface iContent {
  children?: React.ReactNode
}

export function Content({children}: iContent) {
  return (
    <main className={styles.content}>
      {children}
    </main>
  );
}

export default Content