import React, { useContext } from "react";
import styles from "./searchBlock.css";
import { UserBlock } from "./UserBlock";
import { useUserData } from "../../../hooks/useUserData";

export function SearchBlock() {
  const {
    data: { iconImg, name },
    loading,
  } = useUserData();
  return (
    <div className={styles.searchBlock}>
      <UserBlock
        loading={loading}
        userName={name}
        avatarSrc={iconImg?.split("?")[0]}
        href={`https://www.reddit.com/api/v1/authorize?client_id=${window.__clientID__}&response_type=code&state=random_string&redirect_uri=${window.__redirectURL__}&duration=permanent&scope=identity read submit`}
      />
      <div>search</div>
      <div>message</div>
    </div>
  );
}
