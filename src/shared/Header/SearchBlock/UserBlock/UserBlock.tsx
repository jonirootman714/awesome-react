import React from "react";
import styles from "./userBlock.css";
import { EIconName, Icon } from "../../../Icon";
import { EColor, Text } from "../../../Text";

interface IUserBlock {
  avatarSrc?: string;
  userName?: string;
  href?: string;
  loading?: boolean;
}

export function UserBlock({
  avatarSrc,
  userName,
  href = "#",
  loading,
}: IUserBlock) {
  return (
    <a href={href} className={styles.userBlock}>
      <div className={styles.userBlockAvatar}>
        {avatarSrc ? (
          <img
            src={avatarSrc}
            width="50px"
            height="50px"
            alt="user avatar"
            className={styles.userBlockAvatarImg}
          />
        ) : (
          <Icon size={50} name={EIconName.anon} />
        )}
      </div>
      <div className={styles.userBlockName}>
        <Text
          As="p"
          weight={500}
          desktopSize={20}
          color={userName ? EColor.black : EColor.gray99}
        >
          {loading ? "Загрузка..." : userName || "Аноним"}
        </Text>
      </div>
    </a>
  );
}
