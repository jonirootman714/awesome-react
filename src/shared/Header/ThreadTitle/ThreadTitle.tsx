import React from 'react';
import styles from './threadTitle.css';

export function ThreadTitle() {
  return (
    <h1 className={styles.threadTitle}>ThreadTitle</h1>
  );
}

export default ThreadTitle 