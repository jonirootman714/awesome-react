import React, { useEffect, useRef, useState } from "react";
import styles from "./dropdown.css";
import { noop } from "../../utils/js/noop";
import ReactDOM from "react-dom";

interface IDropdownProps {
  button: React.ReactNode;
  children: React.ReactNode;
  isOpen?: boolean;
  onOpen?: () => void;
  onClose?: () => void;
  customClasses?: {
    listWrap?: string;
    listContainer?: string;
  };
}

type Coords = {
  left: number;
  top: number;
  width: number;
};

export function Dropdown({
  button,
  children,
  isOpen,
  onOpen = noop,
  onClose = noop,
  customClasses = { listWrap: "", listContainer: "" },
}: IDropdownProps) {
  const [isDropdownOpen, setIsDropdownOpen] = useState(isOpen);
  const [coords, setCoords] = useState<Coords | null>(null);
  const buttonRef = useRef<HTMLDivElement>(null);
  const getCoords = (): Coords | null => {
    const box = buttonRef.current?.getBoundingClientRect();
    if (box) {
      return {
        left: box.left,
        top: box.top + box.height + window.pageYOffset,
        // top: box.top,
        width: box.width,
      };
    }

    return null;
  };

  useEffect(() => setIsDropdownOpen(isOpen), [isOpen]);

  useEffect(() => (isDropdownOpen ? onOpen() : onClose()), [isDropdownOpen]);

  useEffect(() => {
    if (!isDropdownOpen) return;
    const coords = getCoords();
    setCoords(coords);
  }, [isDropdownOpen]);

  const handlerOpen = () => {
    if (isOpen === undefined) {
      setIsDropdownOpen(!isDropdownOpen);
    }
  };

  const ref = useRef<HTMLDivElement>(null);
  useEffect(() => {
    function handleClick(event: MouseEvent) {
      if (
        event.target instanceof Node &&
        !ref.current?.contains(event.target)
      ) {
        setIsDropdownOpen(false);
      }
    }

    if (isDropdownOpen) {
      document.addEventListener("click", handleClick);
    }

    return () => {
      document.removeEventListener("click", handleClick);
    };
  }, [isDropdownOpen]);

  const node = document.getElementById("dropdown_root");
  if (!node) return null;

  return (
    <div className={styles.container} ref={ref}>
      <div ref={buttonRef} onClick={handlerOpen}>
        {button}
      </div>
      {isDropdownOpen &&
        ReactDOM.createPortal(
          <div
            style={{
              position: "absolute",
              top: coords?.top,
              left: coords?.left,
              // minWidth: coords?.width,
            }}
            className={[
              customClasses.listContainer || "",
              styles.listContainer,
            ].join(" ")}
          >
            <div
              className={[customClasses.listWrap || "", styles.list].join(" ")}
              onClick={() => setIsDropdownOpen(false)}
            >
              {children}
            </div>
          </div>,
          node
        )}
    </div>
  );
}
