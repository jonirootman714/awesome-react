import React from "react";
import styles from "./icon.css";
import { AnonIcon, CommentIcon, HideIcon, SaveIcon, ShareIcon, WarningIcon } from "../Icons";

export enum EIconName {
  comment = "comment",
  share = "share",
  hide = "hide",
  save = "save",
  warning = "warning",
  anon = "anon"
}

interface IIcon {
  name: EIconName;
  className?: string;
  size?: number;
}

export function Icon({ name, className, size = 14 }: IIcon) {
  switch (name) {
    case "comment":
      return <CommentIcon className={className} size={size} />;
    case "share":
      return <ShareIcon className={className} size={size} />;
    case "hide":
      return <HideIcon className={className} size={size} />;
    case "save":
      return <SaveIcon className={className} size={size} />;
    case "warning":
      return <WarningIcon className={className} size={size} />;
    case "anon":
      return <AnonIcon className={className} size={size} />;
    default:
      return <svg></svg>;
  }
}
