import React from "react";
import styles from "./createAt.css";
import { EColor, Text } from "../../../../Text";

interface ICreateAt {
  date: number;
}

export function CreateAt({ date }: ICreateAt) {
  const dateNowNum = Date.now();

  const createdDateNum = dateNowNum - date;
  const createdDate = new Date(createdDateNum);

  const currentDate = new Date();

  let createDateIntl = new Intl.DateTimeFormat("ru-RU", {
    day: "numeric",
    month: "long",
    year: "numeric",
  })
    .format(createdDate)
    .replace(" г.", "");

  if (createdDate.getFullYear() === currentDate.getFullYear()) {
    if (createdDate.getMonth() === currentDate.getMonth()) {
      if (createdDate.getDate() === currentDate.getDate()) {
        const hours = currentDate.getHours() - createdDate.getHours();
        if (hours === 1) {
          createDateIntl = hours + " час назад";
        } else if (hours === 2 || hours === 3 || hours === 4) {
          createDateIntl = hours + " часа назад";
        } else {
          createDateIntl = hours + " часов назад";
        }
      }
    }
  }

  return (
    <Text color={EColor.gray99}>
      <Text color={EColor.gray99} className={styles.publishedLabel}>
        Опубликовано{" "}
      </Text>{" "}
      {createDateIntl}
    </Text>
  );
}
