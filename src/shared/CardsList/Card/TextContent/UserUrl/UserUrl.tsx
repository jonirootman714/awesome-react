import React from "react";
import styles from "./userUrl.css";

interface IUserUrl {
  url: string;
  name: string
}

export function UserUrl(props: IUserUrl) {
  return <a className={styles.userUrl} href={props.url}>{props.name}</a>;
}
