import React from "react";
import styles from "./textContent.css";
import { UserAvatar } from "./UserAvatar";
import { UserUrl } from "./UserUrl";
import { CreateAt } from "./CreateAt";
import { Title } from "./Title";

interface ITextContent {
  title: string;
  created: number;
  author: string;
  userAvatar?: string;
  id: string;
  subreddit: string;
}

export function TextContent({
  title,
  created,
  author,
  userAvatar,
  id,
  subreddit,
}: ITextContent) {
  return (
    <div className={styles.textContent}>
      <div className={styles.textContentMeta}>
        <div className={styles.textContentUserBox}>
          <UserAvatar url={userAvatar || ""} />
          <UserUrl url="#user-url" name={author} />
        </div>
        <CreateAt date={created} />
      </div>
      <Title subreddit={subreddit} id={id} title={title} />
    </div>
  );
}
