import React from "react";
import styles from "./userAvatar.css";

interface IUserAvatar {
  url: string;
}

export function UserAvatar({ url }: IUserAvatar) {
  return (
    <img
      className={styles.userAvatar}
      src={
        url
          ? url
          : "https://images.unsplash.com/photo-1511367461989-f85a21fda167?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1331&q=80"
      }
    />
  );
}
