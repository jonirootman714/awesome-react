import React from "react";
import styles from "./title.css";
import { Text } from "../../../../Text";

import { Link } from "react-router-dom";

interface ITitle {
  title: string;
  id: string;
  subreddit: string;
}

export function Title({ id, title, subreddit }: ITitle) {
  return (
    <Text As="h2" className={styles.title} size={16}>
      <Link className={styles.titleLink} to={`/posts/${subreddit}/${id}`}>
        {title}
      </Link>
    </Text>
  );
}
