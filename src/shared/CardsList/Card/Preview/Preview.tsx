import React from "react";
import styles from "./preview.css";

interface IPreview {
  url: string;
  alt?: string;
}

export function Preview({ url, alt }: IPreview) {
  return (
    <div className={styles.preview}>
      <img
        className={styles.previewImg}
        src={url.replace(
          // eslint-disable-next-line no-useless-escape
          /(\&amp\;)/g,
          "&"
        )}
        alt={alt || "Preview post"}
      />
    </div>
  );
}
