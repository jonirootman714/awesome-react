import React from "react";
import styles from "./card.css";

interface IChildren {
  children?: React.ReactNode;
}

export function Card({ children }: IChildren) {
  return <li className={styles.card}>{children}</li>;
}
