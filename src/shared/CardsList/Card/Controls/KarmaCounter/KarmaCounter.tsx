import React from "react";
import styles from "./karmaCounter.css";

interface IKarmaCounter {
  value?: number;
}

export function KarmaCounter(props: IKarmaCounter) {
  return (
    <div className={styles.karmaCounter}>
      <button className={styles.karmaCounterUpBtn}>
        <svg
          width="19"
          height="10"
          viewBox="0 0 19 10"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M9.5 0L0 10H19L9.5 0Z" fill="#C4C4C4" />
        </svg>
      </button>
      <span className={styles.karmaCounterValue}>{props.value || 0}</span>
      <button className={styles.karmaCounterDownBtn}>
        <svg
          width="19"
          height="10"
          viewBox="0 0 19 10"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M9.5 0L0 10H19L9.5 0Z" fill="#C4C4C4" />
        </svg>
      </button>
    </div>
  );
}
