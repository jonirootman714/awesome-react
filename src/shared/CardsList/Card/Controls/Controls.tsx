import React from "react";
import styles from "./controls.css";
import { KarmaCounter } from "./KarmaCounter";
import { CommentButton } from "./CommentButton";
import { Actions } from "./Actions";

interface IControls {
  score?: number;
  num_comments?: number
}

export function Controls({ score, num_comments }: IControls) {
  return (
    <div className={styles.controls}>
      <KarmaCounter value={score} />
      <CommentButton value={num_comments} />
      <Actions />
    </div>
  );
}
