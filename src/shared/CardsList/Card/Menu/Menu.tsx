import React from "react";
import styles from "./menu.css";
import { Dropdown } from "../../../Dropdown";
import { GenericList } from "../../../../utils/react/genericList";
import { generateId } from "../../../../utils/react/generateRandomIndex";
import { EIconName, Icon } from "../../../Icon";

const listMenuItems = [
  {
    As: "a" as const,
    text: "Комментарии",
    className: styles.listItem,
    href: "#comment",
    iconSvg: (
      <Icon name={EIconName.comment} size={15} className={styles.listItemSvg} />
    ),
  },
  {
    As: "a" as const,
    text: "Поделиться",
    className: styles.listItem,
    href: "#share",
    iconSvg: (
      <Icon name={EIconName.share} size={15} className={styles.listItemSvg} />
    ),
  },
  {
    As: "a" as const,
    text: "Скрыть",
    className: styles.listItem,
    iconSvg: <Icon name={EIconName.hide} className={styles.listItemSvg} />,
  },
  {
    As: "a" as const,
    text: "Сохранить",
    className: styles.listItem,
    href: "#",
    iconSvg: <Icon name={EIconName.save} className={styles.listItemSvg} />,
  },
  {
    As: "a" as const,
    text: "Пожаловаться",
    className: styles.listItem,
    href: "#",
    iconSvg: (
      <Icon name={EIconName.warning} size={16} className={styles.listItemSvg} />
    ),
  },
].map(generateId);

export function Menu() {
  const menuButton = (
    <button className={styles.menuBtn}>
      <svg
        width="5"
        height="20"
        viewBox="0 0 5 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <circle cx="2.5" cy="2.5" r="2.5" fill="#D9D9D9" />
        <circle cx="2.5" cy="10" r="2.5" fill="#D9D9D9" />
        <circle cx="2.5" cy="17.5" r="2.5" fill="#D9D9D9" />
      </svg>
    </button>
  );

  return (
    <div className={styles.menu}>
      <Dropdown
        customClasses={{
          listWrap: styles.dropdownListWrap,
          listContainer: styles.dropdownContainer,
        }}
        button={menuButton}
      >
        <div className={styles.menuContentWrap}>
          <div className={styles.list}>
            <GenericList list={listMenuItems} />
          </div>
          <button className={styles.menuContentBtn}>Закрыть</button>
        </div>
      </Dropdown>
    </div>
  );
}
