import React, { useEffect, useRef, useState } from "react";
import styles from "./cardsList.css";
import { Card } from "./Card";
import { TextContent } from "./Card/TextContent";
import { Preview } from "./Card/Preview";
import { Menu } from "./Card/Menu";
import { Controls } from "./Card/Controls";
import { Text } from "../Text";
import { usePostData } from "../../hooks/usePostData";
import { useDispatch } from "react-redux";
import { postRequestAsync } from "../../store/post/actions";

export interface IPostCardData {
  data: IPostCard;
}

export interface IPostCard {
  title: string;
  author: string;
  score: number;
  num_comments: number;
  sr_detail: {
    created: number;
    icon_img: string;
  };
  preview: { images: [{ source: { url: string } }] };
  id: string;
  subreddit_name_prefixed: string;
}

export function CardsList() {
  const bottomOfList = useRef<HTMLDivElement>(null);
  const dispatch = useDispatch();

  const { posts, loading, error, after } = usePostData("", 8);

  const [nextAfter, setNextAfter] = useState("");
  const [isLoadMore, setIsLoadMore] = useState(false);

  function load() {
    if (nextAfter) dispatch(postRequestAsync(nextAfter, 8));
  }

  useEffect(() => {
    setNextAfter(after);
  }, [after]);

  useEffect(() => {
    if (!posts) return;

    const observer = new IntersectionObserver(
      (entries) => {
        if (entries[0].isIntersecting) {
          if (posts.length === 0) {
            load();
          } else {
            if (posts.length % 16 !== 0) {
              load();
              setIsLoadMore(false);
            } else {
              setIsLoadMore(true);
            }
          }
        }
      },
      {
        rootMargin: "10px",
      }
    );

    if (bottomOfList.current) observer.observe(bottomOfList.current);

    return () => {
      if (bottomOfList.current) observer.unobserve(bottomOfList.current);
    };
  }, [bottomOfList.current, nextAfter]);

  return (
    <ul className={styles.cardsList}>
      {posts.length === 0 && !loading && !error && (
        <Text As="h2" size={28}>
          Нет ни одного поста
        </Text>
      )}

      {posts.map(
        ({
          author,
          id,
          num_comments,
          preview,
          score,
          sr_detail,
          subreddit_name_prefixed,
          title,
        }) => {
          return (
            <Card key={id}>
              <TextContent
                userAvatar={sr_detail.icon_img}
                title={title}
                author={author}
                created={sr_detail.created}
                id={id}
                subreddit={subreddit_name_prefixed}
              />
              <Preview
                url={
                  preview?.images
                    ? preview.images?.[0]?.source.url
                    : "https://images.unsplash.com/photo-1564349683136-77e08dba1ef7?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1172&q=80"
                }
              />
              <Menu />
              <Controls score={score} num_comments={num_comments} />
            </Card>
          );
        }
      )}

      <div ref={bottomOfList} />

      {loading && (
        <Text As="h2" size={28}>
          Загрузка...
        </Text>
      )}

      {error && (
        <Text As="h2" size={28}>
          {error}
        </Text>
      )}

      {isLoadMore && (
        <div className={styles.buttonLoadMoreWrap}>
          <button
            className={styles.buttonLoadMore}
            onClick={() => {
              setIsLoadMore(false);
              load();
            }}
          >
            Загрузить еще
          </button>
        </div>
      )}
    </ul>
  );
}
