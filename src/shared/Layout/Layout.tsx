import React from "react";
import styles from "./layout.css";

interface iLayoutProps {
  children?: React.ReactNode;
}

export function Layout({ children }: iLayoutProps) {
  return <div className={styles.layout}>{children}</div>;
}

export default Layout
