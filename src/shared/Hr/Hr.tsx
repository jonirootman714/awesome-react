import React from "react";
import styles from "./hr.css";
import classNames from "classnames";
import { EColor } from "../Text";

interface IHr {
  color?: EColor;
  margin?: number;
}

export function Hr({ color = EColor.grayD9, margin = 5 }: IHr) {
  const classes = classNames(styles[color]);

  return (
    <hr
      className={classes}
      style={{ marginBottom: margin, marginTop: margin }}
    />
  );
}
