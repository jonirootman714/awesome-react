import React from "react";
import styles from "./sortComment.css";
import { Dropdown } from "../../Dropdown";
import { EColor, Text } from "../../Text";

export function SortComment() {
  return (
    <div className={styles.sort}>
      <Text As="p" color={EColor.gray99} className={styles.sortTitle}>
        Сортировать по:
      </Text>
      <Dropdown
        customClasses={{ listContainer: styles.sortComment }}
        button={
          <button className={styles.sortButton}>
            <Text
              className={styles.sortButtonTitle}
              As={"p"}
              color={EColor.orange}
            >
              Лучшие
            </Text>
            <svg
              width="13"
              height="7"
              viewBox="0 0 13 7"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                // fill-rule="evenodd"
                // clip-rule="evenodd"
                d="M6.5 7L5.57483e-07 0.623141L0.703795 -1.55181e-06L6.5 5.6864L12.2962 -5.38365e-07L13 0.623142L6.5 7Z"
                fill="#CC6633"
              />
            </svg>
          </button>
        }
      >
        <Text As="p">123</Text>
        <Text As="p">123</Text>
        <Text As="p">123</Text>
      </Dropdown>
    </div>
  );
}
