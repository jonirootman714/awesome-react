import React from "react";
import styles from "./postComments.css";
import { PostComment } from "../PostComment/PostComment";
import { IComment } from "../../../hooks/usePostInfo";

interface IPostCommentProps {
  comments: Array<IComment>;
}

export function PostComments({ comments }: IPostCommentProps) {
  // console.log(comments)
  return (
    <div>
      {comments.map(({ author, body_html, id, score }) => {
        return (
          <PostComment
            score={score}
            author={author}
            body={body_html}
            key={id}
          />
        );
      })}
    </div>
  );
}
