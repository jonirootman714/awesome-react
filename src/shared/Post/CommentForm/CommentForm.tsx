import styles from "./commentForm.css";
import React, { ChangeEvent, useEffect, useState } from "react";
import { Field, Form, Formik, FormikHelpers } from "formik";
import { ICommentValues } from "../../CommentFormContainer";

export interface ICommentForm {
  buttonSubmitText?: string;
  placeholder?: string;
  initialValue?: string;
  // handleSubmit?: (event: FormEvent) => void;
  onSubmit?: (
    values: ICommentValues,
    { setSubmitting }: FormikHelpers<ICommentValues>
  ) => void;
  onChange?: (event: ChangeEvent<HTMLTextAreaElement>) => void;
  textareaRef: React.RefObject<HTMLTextAreaElement>;
}

export function CommentForm({
  buttonSubmitText = "Submit",
  placeholder = "",
  initialValue = "",
  // handleSubmit = preventDefault(() => {}),
  onSubmit = (values, { setSubmitting }) => {
    alert(JSON.stringify(values, null, 2));
    setSubmitting(false);
  },
  onChange = () => {},
  textareaRef,
}: ICommentForm) {
  return (
    <div>
      <Formik
        initialValues={{
          comment: initialValue,
        }}
        onSubmit={onSubmit}
      >
        {({ values, handleSubmit, handleChange }) => {
          return (
            <form onSubmit={handleSubmit} className={styles.form}>
              <textarea
                ref={textareaRef}
                placeholder={placeholder}
                className={styles.textArea}
                name="comment"
                value={initialValue}
                onChange={onChange}
              />
              <button className={styles.submit} type="submit">
                {buttonSubmitText}
              </button>
            </form>
          );
        }}
      </Formik>
    </div>
  );
}
