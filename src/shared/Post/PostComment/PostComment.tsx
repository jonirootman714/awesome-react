import React, { useState } from "react";
import styles from "./postComment.css";
import { KarmaCounter } from "../../CardsList/Card/Controls/KarmaCounter";
import { EColor, Text } from "../../Text";
import { UserAvatar } from "../../CardsList/Card/TextContent/UserAvatar";
import { UserUrl } from "../../CardsList/Card/TextContent/UserUrl";
import unescapeHTML from "../../../utils/react/unescapeHTML";
import {
  GenericList,
  IGenericListItem,
} from "../../../utils/react/genericList";
import { EIconName, Icon } from "../../Icon";
import { CommentFormContainer } from "../../CommentFormContainer/CommentFormContainer";

interface IPostComment {
  body: string;
  author: string;
  score: number;
}

export function PostComment({ body, author, score }: IPostComment) {
  const [answerIsOpen, setAnswerIsOpen] = useState(false);
  const actionButtons: Array<IGenericListItem> = [
    {
      id: "1234",
      text: (
        <Text As="p" color={EColor.gray66} size={14}>
          Ответить
        </Text>
      ),
      As: "button",
      iconSvg: (
        <Icon
          name={EIconName.comment}
          size={14}
          className={styles.commentActionButtonSvg}
        />
      ),
      className: styles.commentActionButton,
      onClick: () => {
        setAnswerIsOpen(!answerIsOpen);
      },
    },
  ];

  return (
    <div className={styles.comment}>
      <KarmaCounter value={score}></KarmaCounter>
      <div className={styles.commentInfo}>
        <UserAvatar url="" />
        <UserUrl url="" name={author} />
      </div>
      <div className={styles.commentPipeWrap}>
        <span className={styles.commentPipe}></span>
      </div>
      <Text As="div" color={EColor.black} margin={10} size={14}>
        {unescapeHTML(body)}
      </Text>
      <div>
        <div className={styles.actionButtonsWrap}>
          <GenericList list={actionButtons} />
        </div>
        {answerIsOpen && (
          <CommentFormContainer
            buttonSubmitText="Ответить"
            initialValue={`${author}, `}
          />
        )}
      </div>
    </div>
  );
}
