import React, { useContext, useEffect, useRef } from "react";
import styles from "./post.css";
import ReactDOM from "react-dom";
import { SortComment } from "./SortComment";
import { Hr } from "../Hr";
import { PostComments } from "./PostComments";
import { IPostInfo, usePostInfo } from "../../hooks/usePostInfo";
import { Text } from "../Text";
import unescapeHTML from "../../utils/react/unescapeHTML";
import { CommentFormContainer } from "../CommentFormContainer/CommentFormContainer";
import { useUserData } from "../../hooks/useUserData";
import { useHistory } from "react-router-dom";

interface IPostProps extends IPostInfo {
  
}

export function Post({ postId, subreddit }: IPostProps) {
  const ref = useRef<HTMLDivElement>(null);
  const [{ comments, selftext_html, score, upvote_ratio, title, images }] =
    usePostInfo({
      postId,
      subreddit,
    });

  const history = useHistory();

  const {
    data: { iconImg, name },
    loading,
  } = useUserData();

  useEffect(() => {
    function handleClick(event: MouseEvent) {
      if (
        event.target instanceof Node &&
        !ref.current?.contains(event.target)
      ) {
        history.push("/posts");
      }
    }
    document.addEventListener("click", handleClick);
    return () => {
      document.removeEventListener("click", handleClick);
    };
  }, []);

  const node = document.querySelector("#modal_root");
  if (!node) return null;

  return ReactDOM.createPortal(
    <div className={styles.modal} ref={ref}>
      <div className={styles.modalView}>
        <Text As="h2" margin={16} size={20}>
          {title}
        </Text>

        {images &&
          images.map(({ height, id, url, width }) => (
            <img
              key={id}
              style={{
                maxWidth: width > 960 ? `${800}px` : `${width}px`,
              }}
              src={url?.replace(
                // eslint-disable-next-line no-useless-escape
                /(\&amp\;)/g,
                "&"
              )}
            />
          ))}

        {selftext_html && (
          <Text margin={16} As="div">
            {unescapeHTML(selftext_html)}
          </Text>
        )}

        <Hr margin={10} />

        <CommentFormContainer
          buttonSubmitText="Комментировать"
          placeholder={name + ", оставьте ваш комментарий"}
        />
        <SortComment />

        <Hr margin={10} />

        {comments && <PostComments comments={comments} />}
      </div>
    </div>,
    node
  );
}
