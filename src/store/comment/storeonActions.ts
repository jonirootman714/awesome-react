export let commentText = (store: any) => {
  store.on("@init", () => ({ commentText: "default text" }));

  store.on("setComment", (state: {}, event: string) => {
    console.log(state);
    return { ...state, commentText: event };
  });
};
