import { Reducer } from "react";
import { SET_TOKEN, SetTokenAction } from "./actions";

type TokenActions = SetTokenAction;

interface TokenState {
  token: string;
  isAuth: boolean;
}

export const tokenReducer: Reducer<TokenState, TokenActions> = (
  state,
  action
) => {
  switch (action.type) {
    case SET_TOKEN:
      return {
        ...state,
        token: action.token,
        isAuth: action.token ? true : false,
      };
    default:
      return state;
  }
};
