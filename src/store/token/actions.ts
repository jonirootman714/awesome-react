import { ActionCreator } from "redux";

export const SET_TOKEN = "SET_TOKEN";

export type SetTokenAction = {
  type: typeof SET_TOKEN;
  token: string;
};

export const saveToken: ActionCreator<SetTokenAction> = () => {
  let token = "";
  if (localStorage.getItem("token") !== null) {
    token = JSON.parse(localStorage.getItem("token") || "").token;
  }

  if (window.__token__ !== "undefined" && window.__token__ !== "") {
    token = window.__token__;
    localStorage.setItem("token", JSON.stringify({ token: window.__token__ }));
  }

  return {
    type: SET_TOKEN,
    token,
  };
};
