import {
  ME_REQUEST,
  ME_REQUEST_ERROR,
  ME_REQUEST_SUCCESS,
  MeRequestAction,
  MeRequestErrorAction,
  MeRequestSuccessAction,
} from "./me/actions";
import {
  POST_REQUEST,
  POST_REQUEST_ERROR,
  POST_REQUEST_SUCCESS,
  PostRequestAction,
  PostRequestErrorAction,
  PostRequestSuccessAction,
} from "./post/actions";

import { UPDATE_COMMENT, UpdateCommentAction } from "./comment/actions";
import { SET_TOKEN, SetTokenAction } from "./token/actions";
import { MeState, meReducer } from "./me/reducer";
import { Reducer } from "redux";
import { tokenReducer } from "./token/reducer";
import { PostState, postReducer } from "./post/reducer";

export type RootState = {
  commentText: string;
  token: { token: string; isAuth: boolean };
  me: MeState;
  post: PostState;
};

const initialState: RootState = {
  commentText: "Hi, SkillBox!",
  token: { token: "", isAuth: false },
  me: {
    loading: false,
    error: "",
    data: {},
  },
  post: {
    loading: false,
    error: "",
    data: { posts: [], after: "" },
  },
};

type MyAction =
  | SetTokenAction
  | UpdateCommentAction
  | MeRequestSuccessAction
  | MeRequestAction
  | MeRequestErrorAction
  | PostRequestAction
  | PostRequestErrorAction
  | PostRequestSuccessAction;

export const rootReducer: Reducer<RootState, MyAction> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case UPDATE_COMMENT:
      return {
        ...state,
        commentText: action.text,
      };
    case SET_TOKEN:
      return {
        ...state,
        token: tokenReducer(state.token, action),
      };
    case ME_REQUEST:
    case ME_REQUEST_SUCCESS:
    case ME_REQUEST_ERROR:
      return {
        ...state,
        me: meReducer(state.me, action),
      };

    case POST_REQUEST:
    case POST_REQUEST_SUCCESS:
    case POST_REQUEST_ERROR:
      return {
        ...state,
        post: postReducer(state.post, action),
      };
    default:
      return state;
  }
};
