import { Reducer } from "react";
import {
  POST_REQUEST,
  POST_REQUEST_ERROR,
  POST_REQUEST_SUCCESS,
  PostRequestAction,
  PostRequestErrorAction,
  PostRequestSuccessAction,
} from "./actions";
import { IPostCard } from "../../shared/CardsList";

export type PostState = {
  loading: boolean;
  error: string;
  data: { posts: IPostCard[]; after: string };
};

type PostActions =
  | PostRequestSuccessAction
  | PostRequestAction
  | PostRequestErrorAction;

export const postReducer: Reducer<PostState, PostActions> = (state, action) => {
  switch (action.type) {
    case POST_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case POST_REQUEST_SUCCESS:
      return {
        ...state,
        loading: false,
        data: {
          posts: state.data.posts.concat(...action.data.posts),
          after: action.data.after,
        },
      };
    case POST_REQUEST_ERROR:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
};
