import { Action, ActionCreator } from "redux";
import { ThunkAction } from "redux-thunk";
import { RootState } from "../store";
import axios from "axios";
import { IPostCard, IPostCardData } from "../../shared/CardsList";

export const POST_REQUEST = "POST_REQUEST";
export const POST_REQUEST_SUCCESS = "POST_REQUEST_SUCCESS";
export const POST_REQUEST_ERROR = "POST_REQUEST_ERROR";

export type PostRequestAction = {
  type: typeof POST_REQUEST;
};

export type PostRequestSuccessAction = {
  type: typeof POST_REQUEST_SUCCESS;
  data: { posts: IPostCard[]; after: string };
};

export type PostRequestErrorAction = {
  type: typeof POST_REQUEST_ERROR;
  error: string;
};

export const postRequest: ActionCreator<PostRequestAction> = () => ({
  type: POST_REQUEST,
});

export const postRequestSuccess: ActionCreator<
  PostRequestSuccessAction
> = (data: { posts: IPostCard[]; after: string }) => ({
  type: POST_REQUEST_SUCCESS,
  data,
});

export const postRequestError: ActionCreator<PostRequestErrorAction> = (
  error: string
) => ({
  type: POST_REQUEST_ERROR,
  error,
});

export const postRequestAsync =
  (
    after: string,
    limit = 10
  ): ThunkAction<void, RootState, unknown, Action<string>> =>
  (dispatch, getState) => {
    dispatch(postRequest());
    axios
      .get("https://oauth.reddit.com/best.json", {
        headers: {
          Authorization: `Bearer ${getState().token.token}`,
        },
        params: {
          sr_detail: true,
          limit: limit,
          after,
        },
      })
      .then((response) => {
        const { posts, after }: { posts: IPostCardData[]; after: string } = {
          posts: response.data.data.children,
          after: response.data.data.after,
        };
        const result = posts.map<IPostCard>(
          ({
            data: {
              author,
              id,
              num_comments,
              preview,
              score,
              sr_detail,
              subreddit_name_prefixed,
              title,
            },
          }) => {
            return {
              author,
              id,
              preview,
              num_comments,
              score,
              sr_detail,
              subreddit_name_prefixed,
              title,
            };
          }
        );
        dispatch(postRequestSuccess({ posts: result, after }));
      })
      .catch((error) => {
        dispatch(postRequestError(String(error)));
      });
  };
