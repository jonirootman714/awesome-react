import React = require("react");
import { noop } from "../js/noop";

export interface IGenericListItem {
  id: string;
  text: string | React.ReactNode;
  onClick?: (id: string) => void;
  className?: string;
  As?: "a" | "li" | "button" | "div";
  href?: string;
  iconSvg?: React.ReactNode
}

interface IGenericListProps {
  list: IGenericListItem[];
}

export function GenericList({ list }: IGenericListProps) {
  return (
    <>
      {list.map(({ As = "div", text, onClick = noop, id, href, className, iconSvg = '' }) => (
        <As
          href={href}
          key={id}
          className={className}
          onClick={() => onClick(id)}
        >
          {iconSvg}
          {text}
        </As>
      ))}
    </>
  );
}
