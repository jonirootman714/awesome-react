import express from "express";
import ReactDOM from "react-dom/server";
import "dotenv/config";
const axios = require("axios");

// https://oauth.reddit.com/api/vi/me

import { indexTemplate } from "./indexTemplate";
import { App } from "../AppComponent";
const app = express();

app.use("/static", express.static("./dist/client"));

app.get("/auth", (req, res) => {
  axios
    .post(
      "https://www.reddit.com/api/v1/access_token",
      `grant_type=authorization_code&code=${req.query.code}&redirect_uri=${process.env.REDIRECT_URL}`,
      {
        auth: {
          username: process.env.CLIENT_ID,
          password: process.env.SECRET_KEY,
        },
        headers: { "Content-type": "application/x-www-form-urlencoded" },
      }
    )
    .then(({ data }) => {
      res.send(
        indexTemplate(
          ReactDOM.renderToString(App()),
          data["access_token"],
          process.env.CLIENT_ID,
          process.env.REDIRECT_URL
        )
      );
    })
    .catch(({ data, message }) => {
      console.log(message);
    });

  // res.send(indexTemplate(ReactDOM.renderToString(App())));
});

app.get("*", (req, res) => {
  res.send(
    indexTemplate(
      ReactDOM.renderToString(App()),
      "",
      process.env.CLIENT_ID,
      process.env.REDIRECT_URL
    )
  );
});

app.listen(3000, () => {
  console.log("server started on port http://localhost:3000");
});
