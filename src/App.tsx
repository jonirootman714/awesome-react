import React, { useEffect } from "react";
import { Header } from "./shared/Header/Header";
import { Layout } from "./shared/Layout";
import "./main.global.css";
import { Content } from "./shared/Content";
import { CardsList } from "./shared/CardsList";
import { useDispatch } from "react-redux";
import { saveToken } from "./store/token/actions";
import { Redirect, Route, Switch } from "react-router-dom";
import { Post } from "./shared/Post";
import { Text } from "./shared/Text";

export function AppRoot() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(saveToken());
  }, []);

  return (
    <div>
      <Layout>
        <Header />
        <Content>
          <Switch>
            <Route exact path="/">
              <Text As="h1" size={28}>
                HOME
              </Text>
            </Route>

            <Redirect from="/auth" to="/posts" />
            <Route path="/posts">
              <CardsList />
            </Route>

            <Route path="*">
              <Text As="h1" size={28}>
                Page not fount 404
              </Text>
            </Route>
          </Switch>

          <Route
            path="/posts/:subredditPrefix/:subreddit/:id"
            render={({
              match: {
                params: { id, subreddit, subredditPrefix },
              },
            }) => {
              return (
                <Post
                  subreddit={`${subredditPrefix}/${subreddit}`}
                  postId={id}
                />
              );
            }}
          />
        </Content>
      </Layout>
    </div>
  );
}
