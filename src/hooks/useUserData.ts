import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../store/store";
import { IUserData, meRequestAsync } from "../store/me/actions";
import type {} from "redux-thunk/extend-redux";

export function useUserData(): { data: IUserData; loading: boolean } {
  const data = useSelector<RootState, IUserData>((state) => state.me.data);
  const loading = useSelector<RootState, boolean>((state) => state.me.loading);
  const token = useSelector<RootState, string>((state) => state.token.token);

  const dispatch = useDispatch();

  useEffect(() => {
    if (token === "") return;
    dispatch(meRequestAsync());
  }, [token]);

  return {
    data,
    loading,
  };
}
