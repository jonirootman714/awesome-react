import axios from "axios";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../store/store";

export interface IPostInfo {
  postId: string;
  subreddit: string;
}
export interface IComment {
  body_html: string;
  score: number;
  id: string;
  author: string;
}

interface IPostLayout {
  score: number;
  upvote_ratio: number;
  selftext_html: string;
  title: string;
}

type TPostLayoutPartial = Partial<IPostLayout>;

interface IPostResponseImgSource {
  height: number;
  width: number;
  url: string;
}

type TImagesData = {
  height: number;
  width: number;
  url: string;
  id: string;
}[];

interface IPostResponse extends IPostLayout {
  preview: { images: { id: string; source: IPostResponseImgSource }[] };
}

interface IPostData extends TPostLayoutPartial {
  comments?: IComment[];
  images?: TImagesData;
}

type IPostDataReadonly = Readonly<IPostData>;

export function usePostInfo({ subreddit, postId }: IPostInfo) {
  const [data, setData] = useState<IPostDataReadonly>({});
  const token = useSelector<RootState, string>((state) => state.token.token);

  useEffect(() => {
    Promise.all([
      // https://www.reddit.com/dev/api#GET_comments_{article}
      axios
        .get(`https://oauth.reddit.com/${subreddit}/comments/${postId}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then(({ data }) => {
          const response: { data: IComment }[] = data[1]?.data?.children;
          if (!response) {
            return [];
          }
          const dataResult = response.map(
            ({ data: { author, body_html, id, score } }) => {
              return {
                score: score || 0,
                body_html: body_html || "",
                author: author || "",
                id: id || "",
              };
            }
          );
          return dataResult;
        })
        .catch((error) => {
          console.log(error);
          console.log(error.message);
          console.log(error.code);
        }),
      // https://www.reddit.com/dev/api/#GET_api_info
      axios
        .get(`https://oauth.reddit.com/${subreddit}/api/info`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
          params: {
            id: "t3_" + postId,
          },
        })
        .then(
          ({
            data: {
              data: { children },
            },
          }) => {
            const response: IPostResponse = children[0].data;
            const { preview, title, score, selftext_html, upvote_ratio } =
              response;

            const images: TImagesData = preview?.images.map(
              ({ source, id }) => {
                return { ...source, id };
              }
            );

            return {
              title,
              score,
              upvote_ratio,
              selftext_html,
              images,
            };
          }
        )
        .catch((error) => {
          console.log(error.message);
          console.log(error.code);
        }),
    ]).then((data) => {
      if (data[1]) setData({ ...data[1], comments: data[0] || [] });
    });
  }, [token]);

  return [data];
}
