import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../store/store";
import { IPostCard } from "../shared/CardsList";
import { postRequestAsync } from "../store/post/actions";

export function usePostData(cursorAfter: string, limit: number = 10) {
  const posts = useSelector<RootState, IPostCard[]>(
    (state) => state.post.data.posts
  );
  const after = useSelector<RootState, string>(
    (state) => state.post.data.after
  );
  const loading = useSelector<RootState, boolean>(
    (state) => state.post.loading
  );
  const error = useSelector<RootState, string>((state) => state.post.error);

  const token = useSelector<RootState, string>((state) => state.token.token);
  const dispatch = useDispatch();

  useEffect(() => {
    if (token === "") return;
    dispatch(postRequestAsync(cursorAfter, limit));
  }, [token]);

  return { after, posts, loading, error };
}
