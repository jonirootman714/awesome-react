import React, { useEffect, useState } from "react";
import { hot } from "react-hot-loader/root";
import "./main.global.css";
import { applyMiddleware, createStore } from "redux";
import { Provider } from "react-redux";
import { composeWithDevTools } from "@redux-devtools/extension";
import { rootReducer } from "./store/store";
import { AppRoot } from "./App";
import thunk from "redux-thunk";
import { BrowserRouter } from "react-router-dom";

// -----------------------
import { useStoreon, StoreContext } from "storeon/react";
import { storeonStore } from "./store/storeonStore";

// const Counter = () => {
//   const { dispatch, commentText } = useStoreon("commentText");
//   return (
//     <input
//       onChange={(event) => {
//         // {event.target.value}
//         dispatch("setComment", event.target.value);
//       }}

//       value={commentText}
//     />
//   );
// };

// -----------------------

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

function AppComponent() {
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);
  });

  return (
    <StoreContext.Provider value={storeonStore}>
      <Provider store={store}>
        {mounted && (
          <BrowserRouter>
            <AppRoot />
          </BrowserRouter>
        )}
      </Provider>
    </StoreContext.Provider>
  );
}

export const App = hot(() => <AppComponent />);
