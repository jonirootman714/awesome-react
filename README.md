# ✉ Reddit Mirror

![RDMirror preview](DOC/RDmirror.jpg)

Проект React (TypeScript): новостной сайт с загруженными постами и комментариями (на основе сайта Reddit и API Reddit).

Реализованный функционал:

- Функциональные компоненты;
- Авторизация с использованием публичного API Reddit (получение и сохранение токена);
- Хуки React (useEffect, useState, useRef..);
- Ref и useRef React-hook — реализация механизма щелчка снаружи для скрытия модального окна;
- Custom hooks;
- Передача данных с помощью контекста, рендеринг компонентов с помощью портала;
- Контролируемые и неконтролируемые компоненты на примере поля комментария к посту;
- Загрузка ленты постов;
- Бесконечный список постов;
- Использование Redux в качестве основной библиотеки управления состоянием для этого приложения (actions, reducers, react-redux, redux-thunk);
- Используется дополнительная библиотека управления состоянием `Storeon`;

## Start Project

- IMPORTANT: Use node version 16

### Install dependencies:

```bash
npm i
```

### Dev scripts

- Start dev server http://localhost:3000

```bash
npm run dev
```

- Build dev version

```bash
npm build:dev
```

- Build pre deploy

```bash
npm run predeploy
```

### Production scripts

```bash
npm run build:prod
```

## For developing

Create component

```bash
yo react-ts-component-dir <nameComponent> <path>
```

- Figma design: https://www.figma.com/file/STABzVueKL3brf4aOgkvW2/Rd-(Mirror)
- Reddit Docs:
  - OAuth2 https://github.com/reddit-archive/reddit/wiki/OAuth2
  - app https://www.reddit.com/prefs/apps
